const students = require('./routes/student');
const colleges = require('./routes/college');
const express = require('express');
const {logger,errorLogger} = require('./utils/logging');


const app = express();
app.use(express.json())
app.use(logger);
// here we created one app for both routing college and student
app.use('/students',students);
app.use('/colleges',colleges);
app.use(errorLogger);
port = process.env.PORT || 3000;
const server= app.listen(port,()=>console.log(`listning to port ${port}`));