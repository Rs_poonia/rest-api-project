const Joi = require('joi');

const schemaid = Joi.object({
    id:Joi.number().positive().required()
})
const schemapoststudent = Joi.object({
    id:Joi.number().integer().positive().required(),
    name:Joi.string().required().min(3).max(20),
    collegeid:Joi.number().positive().required(),
    course:Joi.string().required().min(3).max(20),
    address:Joi.string().required().min(3).max(20),
    
})
const schemaputstudent = Joi.object({
    id:Joi.number().integer().positive().required(),
    name:Joi.string().min(3).max(20),
    collegeid:Joi.number(),
    course:Joi.string().min(3).max(20),
    address:Joi.string().min(3).max(20),
    
})
const schemaputcollege = Joi.object({
    id:Joi.number().integer().positive().required(),
    name:Joi.string().min(3).max(20),
    contact:Joi.string().min(10),
    address:Joi.string().min(3).max(20)
})
const schemapostcollege = Joi.object({
    id:Joi.number().integer().positive().required(),
    name:Joi.string().min(3).max(20).required(),
    contact:Joi.string().min(10).required(),
    address:Joi.string().required().min(3).max(20).required()
})

const schemaarea = Joi.object({
    area:Joi.string().required().min(3).max(20)
})

module.exports={
    schemaid,
    schemapoststudent,
    schemaarea,schemaputcollege,schemaputstudent,schemapostcollege
}