const winston = require('winston');

expressWinston = require('express-winston');
module.exports={
logger : expressWinston.logger({
    transports: [
      new winston.transports.File({
        filename:'logger.log'
      })
    ],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    )
  }),
errorLogger : expressWinston.errorLogger({
    transports: [
      new winston.transports.File({
        filename:'errorLogger.log'
      })
    ],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    )
  })
}
