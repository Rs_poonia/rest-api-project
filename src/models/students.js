const { Pool } = require('pg');

const pool = new Pool({
    user:'developer',
    password:'123',
    database:'postgres',
    port:5432,
    host:'localhost'
});

// starting crud operations for students data
readStudentsData=()=>{
    try {
        return pool.query("select * from students ");
    } catch (error) {
        return error;
    }   
}
readStudentDataById=(id)=>{
    try {
        return pool.query("select * from students where id=$1",[id]).then(student=>{
            if(student.rowCount===0){
                throw student
            }
            else {
                return student
            }
        })
    } catch (error) {
        return error;
    }
    
}
async function deleteStudentsDataForId(id){
      try {
        return  await pool.query('delete from Students where id=$1',[id]).then(deleteStudent=>{
            if(deleteStudent.rowCount===0){
                throw deleteStudent;
            }
        })
      } catch (error) {
          return false;
      }   
}
updateStudentsDataForId=(updatestudent)=>{
       try {
        const id=updatestudent.id;
        const name=updatestudent.name;
        const collegeId=updatestudent.collegeid;
        const course=updatestudent.course;
        const address=updatestudent.address;
        console.log(id,name,collegeId,course,address);
    return pool.query('select name,collegeId,course,address from students where id=$1',[id]).then(studentForId=>{  
        if(studentForId.rowCount===0){
            throw studentForId;
        }  else {
        pool.query('update students set name=$1,collegeId=$2,course=$3,address=$4 where id=$5',[name||studentForId.rows[0].name,(collegeId>0 && parseInt(collegeId)!=NaN) ? collegeId:studentForId.rows[0].collegeid, course||studentForId.rows[0].course,address||studentForId.rows[0].address,id])
        }
    })
       } catch (error) {
           return error;
       }       
}
insertNewStudentsData=(newstudent)=>{
    try {
        let sqlquery='insert into Students(id,name,collegeId,course,Address) values($1,$2,$3,$4,$5)'
        return pool.query(sqlquery,newstudent)
    } catch (error) {
        return error;
    }
   
}


module.exports ={
    readStudentsData,
    readStudentDataById,
    deleteStudentsDataForId,
    updateStudentsDataForId,
    insertNewStudentsData
}
// deleteStudentsDataForId(555);