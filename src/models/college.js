const { Pool } = require('pg');

const pool = new Pool({
    user:'developer',
    password:'123',
    database:'postgres',
    port:5432,
    host:'localhost'
});

// stating crud operations in sql on college table

readCollegeData=()=>{
    // return pool.query("select address as location,count(address) as number_of_college from colleges group by address")
  try { 
       return pool.query("select * from colleges")
    } catch(error){
        return error;
    }
}
readCollegeDataAreaWise=(area)=>{
    try {
        return pool.query("select * from colleges where address=$1",[area]).then(colleges=>{
          if(colleges.rowCount===0){
              throw colleges;
          }
          else {
              return colleges;
          }
            
        })
    } catch (error) {
        return error;
    }
    
}
async function deleteCollegeDataForId(id){
    try {
        return await pool.query('delete from colleges where id=$1',[id]);
    } catch (error) {
        return false;
    }
    
    
       
    
}
updateCollegeDataForId=(updateCollegeDataRequest)=>{
    try {
    const id = updateCollegeDataRequest.id;
    // console.log(id);
    const name = updateCollegeDataRequest.name;
    const address = updateCollegeDataRequest.address;
    const contact = updateCollegeDataRequest.contact;
    return pool.query('select name,address,contact from colleges where id=$1', [id]).then(collegedetailforgivenid=>{
        if(collegedetailforgivenid.rowCount===0){
            throw  collegedetailforgivenid;
            // console.log(collegedetailforgivenid.rowCount)
        }
        else{
            pool.query('update colleges set name=$1,address=$2,contact=$3 where id=$4',[name||collegedetailforgivenid.rows[0].name,address||collegedetailforgivenid.rows[0].Address,contact||collegedetailforgivenid.rows[0].contact,id])

        }
          
    })
    } catch (error) {
        // console.log(error)
        return error;
    }
    
}

insertNewCollegeData=(newcollegedata)=>{
    try {
        return pool.query('insert into colleges(id,name,address,contact) values($1,$2,$3,$4)',newcollegedata);
    } catch (error) {
        return error;
    }
    
}

module.exports={
    readCollegeData,
    readCollegeDataAreaWise,
    deleteCollegeDataForId,
    updateCollegeDataForId,
    insertNewCollegeData
};
// let obj={
// 	"id":554,
// 	"name":"iit",
// 	"address":"Mumbai",
// 	"contact":"+919999"

// };
// updateCollegeDataForId(obj).then(e=>console.log(e.rowCount)).catch(err=>console.error(err.rowCount))
// readCollegeDataAreaWise('jodh')