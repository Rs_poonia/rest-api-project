const college = require('../models/college.js');
const express = require('express');
const validation = require('../utils/validation')
router = express.Router();

router.get('/',async (req,res,next)=>{
    const colleges = await college.readCollegeData();
    if(colleges.rowCount===0){
        res.send('there is no data in database');
    }
    res.send(colleges.rows)
}) 

router.get('/:area', (req,res,next)=>{
    const validateArea = validation.schemaarea.validate(req.params);
    if(validateArea.error===null){
        college.readCollegeDataAreaWise(req.params.area).then(areacolleges=>{
        res.send(areacolleges)
        })
        .catch(err=>{ 
            res.status(404).send('Bad Request');
            next('Bad request')
        })
    }
    else {
        res.send('not valid area')
        next('not valid area')
    }
})
router.delete('/:id',async(req,res,next)=>{
    let deleteId = validation.schemaid.validate(req.id);
    if(deleteId.error===null){
        let deleted = await college.deleteCollegeDataForId(req.params.id);
        if(deleted.rowCount===0 || deleted===false){
            res.status(400).send('Bad Request')
            next('Bad Request')
        } 
        else {
            res.status(200).send(deleted);
          }
    }
    else {
        next('Bad Request')
    }
})
router.put('/',(req,res,next)=>{

    const updateId = validation.schemaputcollege.validate(req.body);
    if(updateId.error===null){
        college.updateCollegeDataForId(req.body).then(response=>{
            res.send('successfully updated '+res.status)
        }).catch(err=>{
            res.status(400).send('Bad Request')
            next('Invalid id')
        })
    }
    else {
        res.send(updateId.error.details[0].message)
        next('Invalid id')
    }
     
    // res.send(resu)

    
   
   
})
router.post('/',  (req,res,next)=>{
    const validatenewcollege = validation.schemapostcollege.validate(req.body);
    if(validatenewcollege.error===null){
          college.insertNewCollegeData(Object.values(req.body)).then(inserted=>{
             res.send('successfully updated '+res.status)
         }).catch ((error)=>{
            res.send('can not insert id is already there '+error.code)
         })     
    } else {
        res.send('error')
    }
})
module.exports = router;




