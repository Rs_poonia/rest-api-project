const student = require('../models/students.js');
const express = require('express');
router = express.Router();
const validation = require('../utils/validation')
router.get('/',async (req,res,next)=>{

  const allStudents = await student.readStudentsData();
  if(allStudents.rowCount===0){
    res.status(404).send('Bad Request');
  }
  res.send(allStudents.rows)
})

router.get('/:id',(req,res,next)=>{
  const validateId = validation.schemaid.validate(req.params);
  if(validateId.error===null){
    student.readStudentDataById(req.params.id).then(studentForGivenId=>{
     res.send(studentForGivenId.rows)
     }).catch(err=>{
     res.send('not fund')
     next('error')
     })
  } else {
    res.send(validateId.error.details[0].message)
    next(validateId.error.details[0].message)
  }  
})
router.post('/', (req,res,next)=>{
 
    const validateData = validation.schemapoststudent.validate(req.body)
    if(!validateData.error){
     
      student.insertNewStudentsData(Object.values(req.body)).then(()=>{
        res.send('successfully inserted '+res.status) 
      }).catch((error)=>{
        res.send('can not insert error code '+error.code)
        next('can not insert error code '+error.code)
      })
    }
    else {
      res.send(validateData.error.details[0].message)
      next(validateData.error.details[0].message)
    }
 
})
router.put('/',(req,res,next)=>{
  const validatestudentupdate = validation.schemaputstudent.validate(req.body);
  if(!validatestudentupdate.error){
  student.updateStudentsDataForId(req.body).then(response=>{
    res.send(`updated successfully ${response}`+res.status)
  }).catch(error=>{
    res.status(400).send('Bad Request')
    next('can not update'+error.code)
  })
  } else {
    res.send(validatestudentupdate.error.details[0].message)
    next(validatestudentupdate.error.details[0].message)
}
})
router.delete('/:id', async(req,res,next)=>{
  const validateId = validation.schemaid.validate(req.params);
  if(!validateId.error){
    const result =  await student.deleteStudentsDataForId(req.params.id);
    if(result.rowCount===0||result===false){
      res.status(400).send('Bad Request')
      next('Bad Request '+error.code)
    }
    else {
      res.send('successfully deleted ')
    }
  } else {
    res.send(validateId.error.details[0].message)
    next(validateId.error.details[0].message)
  }
    
})

module.exports = router;
