const { Pool } = require('pg');
const fs = require("fs");
const path = require('path');

const pool = new Pool({
    user:'developer',
    password:'123',
    database:'postgres',
    port:5432,
    host:'localhost'
});

dropTable=()=>{
    return pool.query("DROP TABLE IF EXISTS colleges,students");
}

function creatTableForColleges(){
    // pool.connect();
    return pool.query("CREATE TABLE colleges(id int PRIMARY KEY,name varchar,address varchar,contact varchar)")
}
function insertDataForColleges(){
    let filePath= getPath('./src/csv-files/collegelist.csv');
   return pool.query(`\COPY colleges from '${filePath}' with delimiter ',' csv header`)
}
function getPath(fileName){
    return path.resolve(`${fileName}`);    
}
function createTableForStudets(){
    return pool.query("create table students(id int PRIMARY KEY,name varchar,collegeId int,course varchar,address varchar)")
}
function insertDataForStudents(){
    let filePath = getPath('./src/csv-files/students.csv');
    return pool.query(`\copy students from '${filePath}' with delimiter ',' csv header`)
}
function insertData(){
   dropTable()
    .then(()=> creatTableForColleges())
    .then(()=> insertDataForColleges())
    .then(()=>createTableForStudets())
    .then(()=>insertDataForStudents())
    .catch(err=>console.error(err));
    
}
insertData();